﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test_Project.Models
{
    public class Person
    {
        public int ID { get; set; }
        public String Name { get; set; }
        public int Age { get; set; }
        public String Gender { get; set; }
        public String State { get; set; }
        public DateTime timestamp { get; set; }
    }
}
