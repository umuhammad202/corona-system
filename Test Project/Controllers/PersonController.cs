using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Test_Project.Services;
using Test_Project.Models;

namespace Test_Project.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PersonsController : ControllerBase
    {

        private ILogger _logger;
        private IPersonsService _service;


        public PersonsController(ILogger<PersonsController> logger, IPersonsService service)
        {
            _logger = logger;
            _service = service;

        }

        [HttpGet("/api/cases")]
        public ActionResult<List<Person>> GetAllCoronaCases()
        {
            return _service.GetPersons();
        }

        [HttpGet("/api/cases/{timestamp}/{state?}")]
        public ActionResult<List<Person>> GetAllNewCoronaCasesByFilter(DateTime timestamp, string state)
        {
            return _service.GetPersons(timestamp, state);
        }

        [HttpPost("/api/cases")]
        public ActionResult<Person> AddNewCoronaCase(Person person)
        {
            _service.AddPerson(person);
            return person;
        }

        [HttpPut("/api/cases/{id}/{state}")]
        public ActionResult<Person> UpdateCoronaCase(int id, string state, Person person)
        {
            _service.UpdatePerson(id, state, person);
            return person;
        }
    }
}
