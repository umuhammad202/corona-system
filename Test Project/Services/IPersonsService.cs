﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Test_Project.Models;

namespace Test_Project.Services
{
    public interface IPersonsService
    {
        public List<Person> GetPersons();

        public List<Person> GetPersons(DateTime timestamp, string state);

        public Person AddPerson(Person person);

        public Person UpdatePerson(int id, string state, Person person);
    }
}
