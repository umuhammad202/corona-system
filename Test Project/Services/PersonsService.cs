﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Test_Project.Models;
using Newtonsoft.Json;

namespace Test_Project.Services
{
    public class PersonsService : IPersonsService
    {

        private List<Person> _personList;
        private string dataFile = @"data.json";

        public PersonsService()
        {
            _personList = new List<Person>();
        }

        public List<Person> GetPersons()
        {
            _personList = GetFileData();
            return _personList;
        }

        public List<Person> GetPersons(DateTime timestamp, string state)
        {
            List<Person> _temppersonList = new List<Person>();

            _personList = GetFileData();

            for (var index = 0; index <= _personList.Count - 1; index++)
            {
                Console.WriteLine(_personList[index].ID);
                if (_personList[index].timestamp >= timestamp && _personList[index].State==state)
                {
                    _temppersonList.Add(_personList[index]);
                }
            }

            return _temppersonList;
        }

        public Person AddPerson(Person person)
        {
            _personList.Add(person);

            SetFileData(_personList);

            return person;
        }

        public Person UpdatePerson(int id, string state, Person person)
        {
            int pos = 0;
            _personList = GetFileData();
            for (var index = _personList.Count - 1; index >= 0; index--)
            {
                if (_personList[index].ID == id)
                {
                    _personList[index].State = state;
                    pos = index;
                    break;
                }
            }
            SetFileData(_personList);
            return _personList[pos];
        }

        public void SetFileData(List<Person> person)
        {
            string json = System.Text.Json.JsonSerializer.Serialize(person);

            if (json != null)
            {
                File.WriteAllText(dataFile, json);
            }
        }

        public List<Person> GetFileData()
        {
            List<Person> listPerson = new List<Person>();
            if (File.Exists(dataFile))
            {
                string json = File.ReadAllText(dataFile);

                listPerson = JsonConvert.DeserializeObject<List<Person>>(json);
            }
            return listPerson;   
        }
    }
}
