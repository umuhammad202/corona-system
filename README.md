# Corona System
Test task for the position Full Stack Developer at neotiv

### Prerequisite: 
1) .net framework is setup and installed.
2) npm package is installed.

clone the project by using the command:

    git clone https://git.rwth-aachen.de/umuhammad202/corona-system.git



## For Backend:
1) After cloning the project, open the folder.
2) Right click on `Test Project.sln` and open it on visual studio.
3) Run the project.
4) It will redirect you to `https://localhost:5000/swagger/index.html`
5) The swagger interface show all the API endpoint definitions and schema. You can test the API using swagger.

## For Frontend:

1) Open the command prompt terminal.
2) Change the directory to `corona_app` with the command:  `cd corona_app`
3) Run the command:  `npm run serve`
4) It starts the development server and you can access the application/user interface at `http://localhost:8080/`

## For Data Source:
For data source, the JSON file is used to store the data and can be access at `Test Project/data.json`
